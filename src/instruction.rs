use crate::data_types::*;
use std::convert::TryInto;
use std::fmt;

#[derive(Eq, PartialEq, Copy, Clone)]
pub struct InstructionTime {
    time: u8,
    add_cycle: bool,
}

impl fmt::Debug for InstructionTime {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.add_cycle {
            write!(f, "{}+", self.time)
        } else {
            write!(f, "{}", self.time)
        }
    }
}

impl InstructionTime {
    pub fn new(time: u8, add_cycle: bool) -> Self {
        Self { time, add_cycle }
    }
}

impl From<u8> for InstructionTime {
    fn from(time: u8) -> Self {
        Self {
            time,
            add_cycle: false,
        }
    }
}

#[derive(Eq, PartialEq, Copy, Clone)]
pub enum AddressingMode {
    Implicit,
    Accumulator,
    Immediate(Value),
    ZeroPage(Address8),
    ZeroPageX(Address8),
    ZeroPageY(Address8),
    Absolute(Address16),
    AbsoluteX(Address16),
    AbsoluteY(Address16),
    Indirect(Address16),
    IndirectX(Address8),
    IndirectY(Address8),
    Relative(RelativeAddress8),
}

impl AddressingMode {
    pub fn length(&self) -> u16 {
        use AddressingMode::*;

        match self {
            Implicit => 1,
            Accumulator => 1,
            Immediate(_) => 2,
            ZeroPage(_) => 2,
            ZeroPageX(_) => 2,
            ZeroPageY(_) => 2,
            Absolute(_) => 3,
            AbsoluteX(_) => 3,
            AbsoluteY(_) => 3,
            Indirect(_) => 3,
            IndirectX(_) => 2,
            IndirectY(_) => 2,
            Relative(_) => 2,
        }
    }
}

impl fmt::Debug for AddressingMode {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        use AddressingMode::*;

        match self {
            Implicit => write!(f, ""),
            Accumulator => write!(f, "A"),
            Immediate(value) => write!(f, "#${:02X}", value),
            ZeroPage(value) => write!(f, "${:02X}", value),
            ZeroPageX(value) => write!(f, "${:02X},X", value),
            ZeroPageY(value) => write!(f, "${:02X},Y", value),
            Absolute(value) => write!(f, "${:04X}", value),
            AbsoluteX(value) => write!(f, "${:04X},X", value),
            AbsoluteY(value) => write!(f, "${:04X},Y", value),
            Indirect(value) => write!(f, "(${:04X})", value),
            IndirectX(value) => write!(f, "(${:02X},X)", value),
            IndirectY(value) => write!(f, "(${:02X}),Y", value),
            Relative(value) => write!(f, "${:02X}", value),
        }
    }
}

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum OpCode {
    /// Add with Carry
    ADC,
    /// bitwise AND with accumulator
    AND,
    /// Arithmatic Shift Left
    ASL,
    /// Branch on Carry Clear
    BCC,
    /// Branch on Carry Set
    BCS,
    /// Branch on EQual
    BEQ,
    /// test BITs
    BIT,
    /// Branch on MInus
    BMI,
    /// Branch on Not Equal
    BNE,
    /// Branch on PLus,
    BPL,
    /// BReaK
    BRK,
    /// Branch on Overflow Clear
    BVC,
    /// Branch on Overflow Set
    BVS,
    /// CLear Carry flag
    CLC,
    /// CLear Decimal mode
    CLD,
    /// CLear Interrupt disable flag
    CLI,
    /// CLear oVerflow flag
    CLV,
    /// CoMPare memory with accumulator
    CMP,
    /// ComPare memory and index X
    CPX,
    /// ComPare memory and index Y
    CPY,
    /// DECrement memory
    DEC,
    /// DEcrement index X
    DEX,
    /// DEcrement index Y
    DEY,
    /// Exclusive OR
    EOR,
    /// INCrement memory
    INC,
    /// INcrement index X
    INX,
    /// INcrement index Y
    INY,
    /// JuMP
    JMP,
    /// Jump Saving Return address
    JSR,
    /// LoaD Accumulator
    LDA,
    /// LoaD X register
    LDX,
    /// LoaD Y register
    LDY,
    /// Logical Shift Right
    LSR,
    /// No OPeration
    NOP,
    /// OR memory with Accumulator
    ORA,
    /// PusH Accumulator on stack
    PHA,
    /// PusH Processor status on stack
    PHP,
    /// PuLl Accumulator from stack
    PLA,
    /// PuLl Processor status from stack
    PLP,
    /// Rotate One bit Left
    ROL,
    /// Rotate One bit Right
    ROR,
    /// ReTurn from Interrupt
    RTI,
    /// ReTurn from Subroutine
    RTS,
    /// SuBtraCt memory from accumulator with borrow
    SBC,
    /// SEt Carry flag
    SEC,
    /// SEt Decimal flag
    SED,
    /// SEt Interrupt disable flag
    SEI,
    /// STore Accumulator
    STA,
    /// STore index X
    STX,
    /// STore index Y
    STY,
    /// Transfer Accumulator to index X
    TAX,
    /// Transfer Accumulator to index Y
    TAY,
    /// Transfer Stack pointer to index X
    TSX,
    /// Transfer index X to Accumulator
    TXA,
    /// Transfer index X to Stack pointer
    TXS,
    /// Transfer index Y to Accumulator
    TYA,
}

#[derive(Eq, PartialEq, Copy, Clone)]
pub struct Instruction {
    pub op_code: OpCode,
    pub mode: AddressingMode,
    pub time: u8,
    add_cross_cycle: bool,
    pub unofficial: bool,
    pub op_code_raw: [Option<u8>; 3],
}

impl fmt::Debug for Instruction {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        if self.unofficial {
            write!(f, "*{:?}\t{:?}", self.op_code, self.mode)
        } else {
            write!(f, "{:?}\t{:?}", self.op_code, self.mode)
        }
    }
}

impl Instruction {
    pub fn new_with_raw(
        op_code_raw: [Option<u8>; 3],
        op_code: OpCode,
        mode: AddressingMode,
        time: u8,
        unofficial: bool,
        add_cross_cycle: bool,
    ) -> Self {
        Self {
            op_code,
            mode,
            time,
            op_code_raw,
            unofficial,
            add_cross_cycle,
        }
    }

    pub fn new(op_code: OpCode, mode: AddressingMode, time: u8) -> Self {
        Self {
            op_code,
            mode,
            time,
            op_code_raw: [None; 3],
            unofficial: false,
            add_cross_cycle: false,
        }
    }

    pub fn length(&self) -> u16 {
        self.mode.length()
    }

    pub fn time(&self, cross_page: bool) -> u8 {
        let mut time = self.time;

        if cross_page && self.add_cross_cycle {
            use AddressingMode::*;

            match self.mode {
                AbsoluteX(_) | AbsoluteY(_) | IndirectY(_) => time += 1,
                _ => {}
            }
        }

        time
    }
}

impl From<&Vec<u8>> for Instruction {
    fn from(hex: &Vec<u8>) -> Instruction {
        use AddressingMode::*;
        use OpCode::*;

        let code_raw = [
            hex.get(0).cloned(),
            hex.get(1).cloned(),
            hex.get(2).cloned(),
        ];

        macro_rules! auto_addr {
            (Implicit) => {
                Implicit
            };
            (Accumulator) => {
                Accumulator
            };
            (Immediate) => {
                Immediate(hex[1])
            };
            (ZeroPage) => {
                ZeroPage(hex[1])
            };
            (ZeroPageX) => {
                ZeroPageX(hex[1])
            };
            (ZeroPageY) => {
                ZeroPageY(hex[1])
            };
            (Absolute) => {
                Absolute(addr16!(&hex[1..=2]))
            };
            (AbsoluteX) => {
                AbsoluteX(addr16!(&hex[1..=2]))
            };
            (AbsoluteY) => {
                AbsoluteY(addr16!(&hex[1..=2]))
            };
            (Indirect) => {
                Indirect(addr16!(&hex[1..=2]))
            };
            (IndirectX) => {
                IndirectX(hex[1])
            };
            (IndirectY) => {
                IndirectY(hex[1])
            };
            (Relative) => {
                // If the first bit is 1, the number is negative in this context,
                // so we have to convert it into a negative number on our host system.
                //if hex[1] & (1 << 7) != 0 {
                //Relative(neg!(hex[1]))
                //} else {
                Relative(hex[1] as i8)
                //}
            };
        }

        macro_rules! instr {
            ($op_code:expr, $addr:ident, $time:expr) => {
                Instruction::new_with_raw(
                    code_raw,
                    $op_code,
                    auto_addr!($addr),
                    $time,
                    false,
                    false,
                )
            };
            ($op_code:expr, $addr:ident, $time:expr, +) => {
                Instruction::new_with_raw(code_raw, $op_code, auto_addr!($addr), $time, false, true)
            };

            ($op_code:expr, $addr:ident, $time:expr, *) => {
                Instruction::new_with_raw(code_raw, $op_code, auto_addr!($addr), $time, true, false)
            };
            ($op_code:expr, $addr:ident, $time:expr, +, *) => {
                Instruction::new_with_raw(code_raw, $op_code, auto_addr!($addr), $time, true, true)
            };
        }

        macro_rules! addr16 {
            ($slice:expr) => {
                Address16::from_le_bytes($slice.try_into().unwrap())
            };
        }

        match hex[0] {
            /*
             * Official OpCodes
             */
            // ADC
            0x69 => instr!(ADC, Immediate, 2),
            0x65 => instr!(ADC, ZeroPage, 3),
            0x75 => instr!(ADC, ZeroPageX, 4),
            0x6d => instr!(ADC, Absolute, 4),
            0x7d => instr!(ADC, AbsoluteX, 4, +),
            0x79 => instr!(ADC, AbsoluteY, 4, +),
            0x61 => instr!(ADC, IndirectX, 6),
            0x71 => instr!(ADC, IndirectY, 5, +),

            // AND
            0x29 => instr!(AND, Immediate, 2),
            0x25 => instr!(AND, ZeroPage, 3),
            0x35 => instr!(AND, ZeroPageX, 4),
            0x2d => instr!(AND, Absolute, 4),
            0x3d => instr!(AND, AbsoluteX, 4, +),
            0x39 => instr!(AND, AbsoluteY, 4, +),
            0x21 => instr!(AND, IndirectX, 6),
            0x31 => instr!(AND, IndirectY, 5, +),

            // ASL
            0x0a => instr!(ASL, Accumulator, 2),
            0x06 => instr!(ASL, ZeroPage, 5),
            0x16 => instr!(ASL, ZeroPageX, 6),
            0x0e => instr!(ASL, Absolute, 6),
            0x1e => instr!(ASL, AbsoluteX, 7),

            // BCC
            0x90 => instr!(BCC, Relative, 2, +),

            // BCS
            0xb0 => instr!(BCS, Relative, 2, +),

            // BEQ
            0xf0 => instr!(BEQ, Relative, 2, +),

            // BIT
            0x24 => instr!(BIT, ZeroPage, 3),
            0x2c => instr!(BIT, Absolute, 4),

            // BMI
            0x30 => instr!(BMI, Relative, 2, +),

            // BNE
            0xd0 => instr!(BNE, Relative, 2, +),

            // BPL
            0x10 => instr!(BPL, Relative, 2, +),

            // BRK
            0x00 => instr!(BRK, Implicit, 7),

            // BVC
            0x50 => instr!(BVC, Relative, 2, +),

            // BVS
            0x70 => instr!(BVS, Relative, 2, +),

            // CLC
            0x18 => instr!(CLC, Implicit, 2),

            // CLD
            0xd8 => instr!(CLD, Implicit, 2),

            // CLI
            0x58 => instr!(CLI, Implicit, 2),

            // CLV
            0xb8 => instr!(CLV, Implicit, 2),

            // CMP
            0xc9 => instr!(CMP, Immediate, 2),
            0xc5 => instr!(CMP, ZeroPage, 3),
            0xd5 => instr!(CMP, ZeroPageX, 4),
            0xcd => instr!(CMP, Absolute, 4),
            0xdd => instr!(CMP, AbsoluteX, 4, +),
            0xd9 => instr!(CMP, AbsoluteY, 4, +),
            0xc1 => instr!(CMP, IndirectX, 6),
            0xd1 => instr!(CMP, IndirectY, 5, +),

            // CPX
            0xe0 => instr!(CPX, Immediate, 2),
            0xe4 => instr!(CPX, ZeroPage, 3),
            0xec => instr!(CPX, Absolute, 4),

            // CPY
            0xc0 => instr!(CPY, Immediate, 2),
            0xc4 => instr!(CPY, ZeroPage, 3),
            0xcc => instr!(CPY, Absolute, 4),

            // DEC
            0xc6 => instr!(DEC, ZeroPage, 5),
            0xd6 => instr!(DEC, ZeroPageX, 6),
            0xce => instr!(DEC, Absolute, 6),
            0xde => instr!(DEC, AbsoluteX, 7, +),

            // DEX
            0xca => instr!(DEX, Implicit, 2),

            // DEY
            0x88 => instr!(DEY, Implicit, 2),

            // EOR
            0x49 => instr!(EOR, Immediate, 2),
            0x45 => instr!(EOR, ZeroPage, 3),
            0x55 => instr!(EOR, ZeroPageX, 4),
            0x4d => instr!(EOR, Absolute, 4),
            0x5d => instr!(EOR, AbsoluteX, 4, +),
            0x59 => instr!(EOR, AbsoluteY, 4, +),
            0x41 => instr!(EOR, IndirectX, 6),
            0x51 => instr!(EOR, IndirectY, 5, +),

            // INC
            0xe6 => instr!(INC, ZeroPage, 5),
            0xf6 => instr!(INC, ZeroPageX, 6),
            0xee => instr!(INC, Absolute, 6),
            0xfe => instr!(INC, AbsoluteX, 7, +),

            // INX
            0xe8 => instr!(INX, Implicit, 2),

            // INY
            0xc8 => instr!(INY, Implicit, 2),

            // JMP
            0x4c => instr!(JMP, Absolute, 3),
            0x6c => instr!(JMP, Indirect, 5),

            // JSR
            0x20 => instr!(JSR, Absolute, 6),

            // LDA
            0xa9 => instr!(LDA, Immediate, 2),
            0xa5 => instr!(LDA, ZeroPage, 3),
            0xb5 => instr!(LDA, ZeroPageX, 4),
            0xad => instr!(LDA, Absolute, 4),
            0xbd => instr!(LDA, AbsoluteX, 4, +),
            0xb9 => instr!(LDA, AbsoluteY, 4, +),
            0xa1 => instr!(LDA, IndirectX, 6),
            0xb1 => instr!(LDA, IndirectY, 5, +),

            // LDX
            0xa2 => instr!(LDX, Immediate, 2),
            0xa6 => instr!(LDX, ZeroPage, 3),
            0xb6 => instr!(LDX, ZeroPageY, 4),
            0xae => instr!(LDX, Absolute, 4),
            0xbe => instr!(LDX, AbsoluteY, 4, +),

            // LDY
            0xa0 => instr!(LDY, Immediate, 2),
            0xa4 => instr!(LDY, ZeroPage, 3),
            0xb4 => instr!(LDY, ZeroPageX, 4),
            0xac => instr!(LDY, Absolute, 4),
            0xbc => instr!(LDY, AbsoluteX, 4, +),

            // LSR
            0x4a => instr!(LSR, Accumulator, 2),
            0x46 => instr!(LSR, ZeroPage, 5),
            0x56 => instr!(LSR, ZeroPageX, 6),
            0x4e => instr!(LSR, Absolute, 6),
            0x5e => instr!(LSR, AbsoluteX, 7),

            // NOP
            0xea => instr!(NOP, Implicit, 2),

            // ORA
            0x09 => instr!(ORA, Immediate, 2),
            0x05 => instr!(ORA, ZeroPage, 3),
            0x15 => instr!(ORA, ZeroPageX, 4),
            0x0d => instr!(ORA, Absolute, 4),
            0x1d => instr!(ORA, AbsoluteX, 4, +),
            0x19 => instr!(ORA, AbsoluteY, 4, +),
            0x01 => instr!(ORA, IndirectX, 6),
            0x11 => instr!(ORA, IndirectY, 5, +),

            // PHA
            0x48 => instr!(PHA, Implicit, 3),

            // PHP
            0x08 => instr!(PHP, Implicit, 3),

            // PLA
            0x68 => instr!(PLA, Implicit, 4),

            // PLP
            0x28 => instr!(PLP, Implicit, 4),

            // ROL
            0x2a => instr!(ROL, Accumulator, 2),
            0x26 => instr!(ROL, ZeroPage, 5),
            0x36 => instr!(ROL, ZeroPageX, 6),
            0x2e => instr!(ROL, Absolute, 6),
            0x3e => instr!(ROL, AbsoluteX, 7),

            // ROR
            0x6a => instr!(ROR, Accumulator, 2),
            0x66 => instr!(ROR, ZeroPage, 5),
            0x76 => instr!(ROR, ZeroPageX, 6),
            0x6e => instr!(ROR, Absolute, 6),
            0x7e => instr!(ROR, AbsoluteX, 7),

            // RTI
            0x40 => instr!(RTI, Implicit, 6),

            // RTS
            0x60 => instr!(RTS, Implicit, 6),

            // SBC
            0xe9 => instr!(SBC, Immediate, 2),
            0xe5 => instr!(SBC, ZeroPage, 3),
            0xf5 => instr!(SBC, ZeroPageX, 4),
            0xed => instr!(SBC, Absolute, 4),
            0xfd => instr!(SBC, AbsoluteX, 4, +),
            0xf9 => instr!(SBC, AbsoluteY, 4, +),
            0xe1 => instr!(SBC, IndirectX, 6),
            0xf1 => instr!(SBC, IndirectY, 5, +),

            // SEC
            0x38 => instr!(SEC, Implicit, 2),

            // SED
            0xf8 => instr!(SED, Implicit, 2),

            // SEI
            0x78 => instr!(SEI, Implicit, 2),

            // STA
            0x85 => instr!(STA, ZeroPage, 3),
            0x95 => instr!(STA, ZeroPageX, 4),
            0x8d => instr!(STA, Absolute, 4),
            0x9d => instr!(STA, AbsoluteX, 5),
            0x99 => instr!(STA, AbsoluteY, 5),
            0x81 => instr!(STA, IndirectX, 6),
            0x91 => instr!(STA, IndirectY, 6),

            // STX
            0x86 => instr!(STX, ZeroPage, 3),
            0x96 => instr!(STX, ZeroPageY, 4),
            0x8e => instr!(STX, Absolute, 4),

            // STY
            0x84 => instr!(STY, ZeroPage, 3),
            0x94 => instr!(STY, ZeroPageX, 4),
            0x8c => instr!(STY, Absolute, 4),

            // TAX
            0xaa => instr!(TAX, Implicit, 2),

            // TAY
            0xa8 => instr!(TAY, Implicit, 2),

            // TSX
            0xba => instr!(TSX, Implicit, 2),

            // TXA
            0x8a => instr!(TXA, Implicit, 2),

            // TXS
            0x9a => instr!(TXS, Implicit, 2),

            // TYA
            0x98 => instr!(TYA, Implicit, 2),

            // NOP (unofficial)
            0x04 | 0x44 | 0x64 => instr!(NOP, ZeroPage, 3, *),
            0x0c => instr!(NOP, Absolute, 4, *),

            _ => {
                //println!("Unknown OpCode: {:#x}", hex[0]);
                instr!(NOP, Implicit, 2)
            }
        }
    }
}

#[cfg(test)]
mod hex_to_instruction {
    use super::*;
    use AddressingMode::*;
    use OpCode::*;

    macro_rules! instr_test {
        ($($hex:expr),+ $(,)? => $instr:expr) => {
            assert_eq!(Instruction::from(&vec![$($hex as u8),+]), $instr)
        };
    }

    macro_rules! instr_new {
        ($($arg:expr),+ $(,)?) => {
            Instruction::new($($arg),+)
        };
    }

    #[test]
    fn adc() {
        instr_test!(0x69, 0x42 => instr_new!(ADC, Immediate(0x42), 2));
        instr_test!(0x65, 0x42 => instr_new!(ADC, ZeroPage(0x42), 3));
        instr_test!(0x71, 0x44 => instr_new!(ADC, IndirectY(0x44), 5));
    }

    #[test]
    fn and() {
        instr_test!(0x3d, 0x00, 0x44 => instr_new!(AND, AbsoluteX(0x4400), 4));
    }

    #[test]
    fn bmi() {
        instr_test!(0x30, 0xfc => instr_new!(BMI, Relative(-4), 3));
        instr_test!(0x30, 0x03 => instr_new!(BMI, Relative(3), 3));
    }
}
