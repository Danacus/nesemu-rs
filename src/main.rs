use nesemu::CPU;
use nesemu::instruction::*;
use nesemu::loader::*;

fn main() {
    nestest();
    //functional_test_bin();
    //bubblesort();
}

fn nestest() {
    let mut cpu = CPU::new();

    INESLoader::load(&mut cpu, std::fs::read("nestest.nes").unwrap().as_slice()); 

    loop {
        cpu.run_cycle();
    }
}

fn functional_test_bin() {
    let mut cpu = CPU::new();

    MOS6502Loader::load(&mut cpu, std::fs::read("testrom2").unwrap().as_slice()); 

    loop {
        cpu.run_cycle();
    }
}

fn bubblesort() {
    let mut cpu = CPU::new();
    cpu.load_data(0x400, std::fs::read("bubblesort").unwrap().as_slice());

    cpu.load_data(0x30, &[0x00, 0x02]); // Data at 0x0200

    cpu.load_data(0x200, &[
        0x10, // Lenght of list
        5, 3, 7, 1, 9, 2, 6, 2, 3, 6,
    ]);

    cpu.run_instruction(Instruction::new(OpCode::JSR, AddressingMode::Absolute(0x400), 6.into())); 

    while cpu.run_cycle() {}

    println!("{:#x?}", cpu.dump_mem(0x207, 0x210));
}
