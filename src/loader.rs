use crate::cpu::CPU;
use crate::data_types::*;
use crate::mapper::*;
use crate::ppu::Mirroring;
use crate::ppu::PPU;

pub trait Loader {
    fn load(cpu: &mut CPU, bytes: &[u8]);
}

/// Loads data in RAM at 0x400
pub struct MOS6502Loader;

impl Loader for MOS6502Loader {
    fn load(cpu: &mut CPU, bytes: &[u8]) {
        let mapper = MOS6502Mapper::new();
        cpu.bus.add_mapper(mapper);
        cpu.load_data(0x400, bytes);
        cpu.set_pc(0x400);
    }
}

#[derive(Debug)]
pub struct INESHeader {
    prg_rom_size: u8,
    chr_rom_size: u8,
    mapper: u8,
    mirroring: bool,
    battery_prg_ram: bool,
    trainer: bool,
    ignore_mirroring: bool,
    prg_ram_size: u8,
}

pub struct INESLoader;

impl INESLoader {
    fn parse_header(bytes: Vec<u8>) -> INESHeader {
        let mut iter = bytes.iter().cloned();

        assert_eq!(iter.next(), Some(0x4e)); // 0: N
        assert_eq!(iter.next(), Some(0x45)); // 1: E
        assert_eq!(iter.next(), Some(0x53)); // 2: S
        assert_eq!(iter.next(), Some(0x1a)); // 3: \n\r

        let prg_rom_size = iter.next().unwrap(); // 4
        let chr_rom_size = iter.next().unwrap(); // 5

        let flag6 = iter.next().unwrap(); // 6
        let mut mapper = (flag6 & 0xF) >> 4; // Lower nybble
        let mirroring = (flag6 & (1 << 0)) != 0;
        let battery_prg_ram = (flag6 & (1 << 1)) != 0;
        let trainer = (flag6 & (1 << 2)) != 0;
        let ignore_mirroring = (flag6 & (1 << 3)) != 0;

        let flag7 = iter.next().unwrap(); // 7
        mapper += flag7 & 0xF; // Upper nybble

        let prg_ram_size = iter.next().unwrap(); // 8

        INESHeader {
            prg_rom_size,
            chr_rom_size,
            mapper,
            mirroring,
            battery_prg_ram,
            trainer,
            ignore_mirroring,
            prg_ram_size,
        }
    }
}

impl Loader for INESLoader {
    fn load(cpu: &mut CPU, bytes: &[u8]) {
        let header = Self::parse_header(bytes.iter().take(16).cloned().collect());

        println!("{:#?}", header);

        let iter = if header.trainer {
            bytes.iter().cloned().skip(16 + 512)
        } else {
            bytes.iter().cloned().skip(16)
        };

        let prg_size = 0x4000 * header.prg_rom_size as usize;
        let prg = iter.clone().take(prg_size).collect::<Vec<_>>();
        let iter = iter.clone().skip(prg_size);

        let chr_size = 0x2000 * header.chr_rom_size as usize;
        let chr = iter.clone().take(chr_size).collect::<Vec<_>>();

        let m = (header.ignore_mirroring, header.mirroring);
        let mirroring = match m {
            (true, _) => Mirroring::FourScreen,
            (false, false) => Mirroring::Horizontal,
            (false, true) => Mirroring::Vertical,
        };

        let mapper = NESMapper::new(mirroring, chr.as_slice());
        cpu.bus.add_mapper(mapper);

        let mapper = match header.mapper {
            0x0 => Mapper0::new(prg.as_slice()),
            _ => Mapper0::new(prg.as_slice()),
            //_ => panic!("Unknown mapper: {}", header.mapper),
        };

        cpu.bus.add_mapper(mapper);
    }
}
