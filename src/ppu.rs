use crate::data_types::*;
use std::any::Any;

const PPUCTRL: Address16 = 0x2000;
const PPUMASK: Address16 = 0x2001;
const PPUSTATUS: Address16 = 0x2002;
const OAMADDR: Address16 = 0x2003;
const OAMDATA: Address16 = 0x2004;
const PPUSCROLL: Address16 = 0x2005;
const PPUADDR: Address16 = 0x2006;
const PPUDATA: Address16 = 0x2007;
const OAMDMA: Address16 = 0x4014;

macro_rules! bit {
    ($byte:expr, $num:expr) => {
        ($byte & (1 << $num)) != 0
    };
}

pub enum Mirroring {
    Horizontal,
    Vertical,
    SingleScreen,
    FourScreen,
}

pub struct Control {
    nmi: bool,
    master: bool,
    sprite_height: bool,
    pub background_table: bool,
    sprite_table: bool,
    increment: bool,
    nametable: u8,
}

impl Control {
    pub fn new() -> Self {
        Self {
            nmi: false,
            master: false,
            sprite_height: false,
            background_table: false,
            sprite_table: false,
            increment: false,
            nametable: 0x00,
        }
    }

    pub fn set_from_byte(&mut self, byte: u8) {
        self.nmi = bit!(byte, 7);
        self.master = bit!(byte, 6);
        self.sprite_height = bit!(byte, 5);
        self.background_table = bit!(byte, 4);
        self.sprite_table = bit!(byte, 3);
        self.increment = bit!(byte, 2);
        self.nametable = byte & 0x3;
    }

    pub fn increment(&self) -> Value {
        match self.increment {
            false => 1,
            true => 32,
        }
    }
}

pub struct Mask {
    blue: bool,
    green: bool,
    red: bool,
    sprite_enable: bool,
    background_enable: bool,
    sprite_left_column_enable: bool,
    background_left_column_enable: bool,
    greyscale: bool,
}

impl Mask {
    pub fn new() -> Self {
        Self {
            blue: false,
            green: false,
            red: false,
            sprite_enable: false,
            background_enable: false,
            sprite_left_column_enable: false,
            background_left_column_enable: false,
            greyscale: false,
        }
    }

    pub fn set_from_byte(&mut self, byte: u8) {
        self.blue = bit!(byte, 7);
        self.green = bit!(byte, 6);
        self.red = bit!(byte, 5);
        self.sprite_enable = bit!(byte, 4);
        self.background_enable = bit!(byte, 3);
        self.sprite_left_column_enable = bit!(byte, 2);
        self.background_left_column_enable = bit!(byte, 1);
        self.greyscale = bit!(byte, 0);
    }
}

pub struct Status {
    vblank: bool,
    sprite_0_hit: bool,
    sprite_overflow: bool,
}

struct Address {
    lo: Value,
    hi: Value,
    is_high: bool,
}

impl Address {
    pub fn new() -> Self {
        Self {
            lo: 0,
            hi: 0,
            is_high: false,
        }
    }

    pub fn set(&mut self, address: Address16) {
        self.hi = ((address & 0xFF00) >> 8) as Value;
        self.lo = (address & 0x00FF) as Value;
    }

    pub fn get(&self) -> Address16 {
        ((self.hi as u16) << 8) | self.lo as u16
    }

    pub fn write_value(&mut self, value: Value) {
        if self.is_high {
            self.lo = value;
        } else {
            self.hi = value;
        }

        self.is_high = !self.is_high;
    }

    pub fn increment(&mut self, inc: Value) {
        let lo = self.lo;
        self.lo = self.lo.wrapping_add(inc);
        if lo > self.lo {
            self.hi = self.hi.wrapping_add(1);
        }
    }
}

impl Status {
    pub fn new() -> Self {
        Self {
            vblank: true,
            sprite_0_hit: false,
            sprite_overflow: true,
        }
    }

    pub fn set_from_byte(&mut self, byte: u8) {
        self.vblank = bit!(byte, 7);
        self.sprite_0_hit = bit!(byte, 6);
        self.sprite_overflow = bit!(byte, 5);
    }

    pub fn get_byte(&self) -> u8 {
        let mut byte = 0x0;

        if self.vblank {
            byte += 1 << 7
        };
        if self.sprite_0_hit {
            byte += 1 << 6
        };
        if self.sprite_overflow {
            byte += 1 << 5
        };

        byte
    }
}

pub struct PPUMapper {
    chr_rom: ROM,
    vram: RAM,
    palette_ram: RAM,
    mirroring: Mirroring,
}

impl PPUMapper {
    pub fn new(mirroring: Mirroring, chr: &[u8]) -> Self {
        Self {
            chr_rom: ROM::new(chr),
            vram: RAM::new(0x1000),
            palette_ram: RAM::new(0xFF),
            mirroring,
        }
    }

    // I admit being too lazy to write this piece of code, and I stole it
    fn mirror_vram_addr(&self, addr: u16) -> u16 {
        let mirrored_vram = addr & 0b10111111111111; // mirror down 0x3000-0x3eff to 0x2000 - 0x2eff
        let vram_index = mirrored_vram - 0x2000; // to vram vector
        let name_table = vram_index / 0x400; // to the name table index
        match (&self.mirroring, name_table) {
            (Mirroring::Vertical, 2) | (Mirroring::Vertical, 3) => vram_index - 0x800,
            (Mirroring::Horizontal, 2) => vram_index - 0x400,
            (Mirroring::Horizontal, 1) => vram_index - 0x400,
            (Mirroring::Horizontal, 3) => vram_index - 0x800,
            _ => vram_index,
        }
    }
}

impl Memory for PPUMapper {
    fn read(&mut self, address: Address16) -> Option<Value> {
        match address {
            0x0000..=0x1FFF => self.chr_rom.read(address),
            0x2000..=0x3EFF => self.vram.read(self.mirror_vram_addr(address)),
            0x3F00..=0x3FFF => self.palette_ram.read(address - 0x3F00),
            _ => None,
        }
    }

    fn write(&mut self, address: Address16, data: Value) {
        match address {
            0x0000..=0x1FFF => self.chr_rom.write(address, data),
            0x2000..=0x3EFF => self.vram.write(self.mirror_vram_addr(address), data),
            0x3F00..=0x3FFF => self.palette_ram.write(address - 0x3F00, data),
            _ => {}
        }
    }
}

pub struct PPU {
    pub ppu_ctrl: Control,
    ppu_mask: Mask,
    ppu_status: Status,
    oam_addr: Address8,
    ppu_scroll: Value,
    ppu_addr: Address,
    ppu_data: Value,
    oam_dma: Address8,

    nmi: bool,

    pub oam_ram: RAM,
    pub mapper: PPUMapper,

    pub cycles: usize,
    pub scanlines: usize,

    pub dirty_read: bool,
}

impl PPU {
    pub fn new(mirroring: Mirroring, chr_rom: &[u8]) -> Self {
        Self {
            ppu_ctrl: Control::new(),
            ppu_mask: Mask::new(),
            ppu_status: Status::new(),
            oam_addr: 0x00,
            ppu_scroll: 0x00,
            ppu_addr: Address::new(),
            ppu_data: 0x00,
            oam_dma: 0x00,

            nmi: false,

            oam_ram: RAM::new(256),
            mapper: PPUMapper::new(mirroring, chr_rom),

            cycles: 0,
            scanlines: 0,

            dirty_read: false,
        }
    }

    pub fn write_to_ctrl(&mut self, value: Value) {
        let nmi_old = self.ppu_ctrl.nmi;
        self.ppu_ctrl.set_from_byte(value);
        if !nmi_old && self.ppu_ctrl.nmi && self.ppu_status.vblank {
            self.nmi = true;
        }
    }

    pub fn dump_mem(&mut self, start: Address16, end: Address16) -> Vec<u8> {
        let mut res = Vec::new();

        for i in start..=end {
            res.push(self.view_mem(i));
        }

        res
    }

    pub fn write_oam(&mut self, address: Address8, data: Value) {
        self.oam_ram.write(address as u16, data);
    }

    pub fn write_oam_data(&mut self, value: Value) {
        self.oam_ram.write(self.oam_addr as u16, value);
        self.oam_addr = self.oam_addr.wrapping_add(1);
    }

    pub fn write_mem(&mut self, data: Value) {
        self.mapper.write(self.ppu_addr.get(), data);
        //println!("{:04X}", self.ppu_addr.get());
        self.ppu_addr.increment(self.ppu_ctrl.increment());
    }

    pub fn read_mem(&mut self) {
        self.ppu_data = self.mapper.read(self.ppu_addr.get()).unwrap_or(0);
        if !self.dirty_read {
            self.ppu_addr.increment(self.ppu_ctrl.increment());
        }
    }

    pub fn view_mem(&mut self, address: Address16) -> Value {
        self.mapper.read(address).unwrap_or(0)
    }

    pub fn poll_nmi(&mut self) -> bool {
        let result = self.nmi;
        self.nmi = false;
        result
    }

    pub fn tick(&mut self, cycles: u8) {
        self.cycles += cycles as usize;
        if self.cycles >= 341 {
            self.cycles -= 341;
            self.scanlines += 1;

            if self.scanlines == 241 {
                if self.ppu_ctrl.nmi {
                    self.ppu_status.vblank = true;
                    self.nmi = true;
                }
            }

            if self.scanlines >= 262 {
                self.scanlines = 0;
                self.ppu_status.vblank = false;
            }
        }
    }
}

impl Memory for PPU {
    fn write(&mut self, address: Address16, data: Value) {
        //println!("Write: {:#x}", address);

        match address {
            PPUCTRL => {
                self.write_to_ctrl(data);
            }
            PPUMASK => self.ppu_mask.set_from_byte(data),
            OAMADDR => self.oam_addr = data,
            OAMDATA => self.write_oam_data(data),
            PPUSCROLL => self.ppu_scroll = data,
            PPUADDR => self.ppu_addr.write_value(data),
            PPUDATA => self.write_mem(data),
            OAMDMA => self.oam_dma = data,
            _ => {}
        }
    }

    fn read(&mut self, address: Address16) -> Option<Value> {
        //println!("Read: {:#x}", address);

        match address {
            PPUSTATUS => Some(self.ppu_status.get_byte()),
            OAMDATA => self.oam_ram.read(self.oam_addr as u16),
            PPUDATA => {
                let data = self.ppu_data;
                self.read_mem();
                Some(data)
            }
            _ => None,
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_from_byte() {
        let mut ppu = PPU::new(Mirroring::FourScreen, &[]);
        ppu.ppu_ctrl.set_from_byte(0b00101010);
        assert_eq!(ppu.ppu_ctrl.sprite_height, true);
        assert_eq!(ppu.ppu_ctrl.sprite_table, true);
        assert_eq!(ppu.ppu_ctrl.nametable, 0x2);

        ppu.ppu_status.set_from_byte(0b01000000);
        assert_eq!(ppu.ppu_status.get_byte(), 0b01000000);
    }

    #[test]
    fn test_ppu_addr() {
        let mut ppu = PPU::new(Mirroring::FourScreen, &[]);
        ppu.write(PPUADDR, 0x06);
        ppu.write(PPUADDR, 0x01);
        assert_eq!(ppu.ppu_addr.get(), 0x0601);

        ppu.write(PPUDATA, 0x69);
        assert_eq!(ppu.ppu_addr.get(), 0x0602);
        assert_eq!(ppu.view_mem(0x0601), 0x69);

        ppu.write(PPUADDR, 0x06);
        ppu.write(PPUADDR, 0x01);
        assert_eq!(ppu.ppu_addr.get(), 0x0601);
        assert_eq!(ppu.ppu_data, 0x00);

        assert_eq!(ppu.view_mem(0x0601), 0x69);

        let data = ppu.read(PPUDATA);
        assert_eq!(ppu.ppu_addr.get(), 0x0602);
        assert_eq!(ppu.ppu_data, 0x69);
        assert_eq!(data.unwrap(), 0x00);

        let data = ppu.read(PPUDATA);
        assert_eq!(ppu.ppu_addr.get(), 0x0603);
        assert_eq!(ppu.ppu_data, 0x00);
        assert_eq!(data.unwrap(), 0x69);
    }

    #[test]
    fn test_ppu_oam() {
        let mut ppu = PPU::new(Mirroring::FourScreen, &[]);
        ppu.write(OAMADDR, 12);
        assert_eq!(ppu.oam_addr, 12);

        ppu.write(OAMDATA, 69);

        let data = ppu.read(OAMDATA);
        assert_eq!(data.unwrap(), 0);

        ppu.write(OAMADDR, 12);
        assert_eq!(ppu.oam_addr, 12);

        let data = ppu.read(OAMDATA);
        assert_eq!(data.unwrap(), 69);
    }
}
