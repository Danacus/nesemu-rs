use crate::data_types::*;

pub struct APU {

}

impl Memory for APU {
    fn write(&mut self, address: Address16, data: Value) {}
    fn read(&mut self, address: Address16) -> Option<Value> { None }
}
