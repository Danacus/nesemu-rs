use nesemu::debugger::NESEmulator;

fn main() {
    let native_options = eframe::NativeOptions::default();
    eframe::run_native(
        "NES Emulator",
        native_options,
        Box::new(|cc| Box::new(NESEmulator::new(cc))),
    );
}
