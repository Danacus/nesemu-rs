//use macroquad::prelude::*;
use crate::*;
use eframe::egui;
use std::cell::RefCell;
use std::fs::OpenOptions;
use std::io::prelude::*;
use std::rc::Rc;

fn get_log_hook(log: Rc<RefCell<String>>) -> Box<dyn Fn(String)> {
    log.borrow_mut()
        .push_str("Log hook initialized successfully!\n");

    return Box::new(move |message| {
        let line = format!("{}\n", &message);
        log.borrow_mut().push_str(&line);
    });
}

fn get_instruction_log_hook(log: Rc<RefCell<String>>) -> Box<dyn FnMut(String)> {
    let mut file = OpenOptions::new()
        .write(true)
        .create(true)
        .open(format!(
            "instruction-log-{}",
            std::time::SystemTime::now()
                .duration_since(std::time::SystemTime::UNIX_EPOCH)
                .unwrap()
                .as_secs()
        ))
        .unwrap();

    return Box::new(move |message| {
        let _ = file.write(message.as_bytes());
        log.borrow_mut().push_str(&message);
    });
}

#[derive(PartialEq)]
enum LoaderSelection {
    MOS6502,
    NES,
}

pub fn load_6502_rom(mut cpu: &mut CPU, filename: &std::path::PathBuf) {
    MOS6502Loader::load(&mut cpu, std::fs::read(filename).unwrap().as_slice());
    log!(cpu, "ROM Loaded at 0x400",);
    cpu.reset();
}

pub fn load_nes_rom(mut cpu: &mut CPU, filename: &std::path::PathBuf) {
    INESLoader::load(&mut cpu, std::fs::read(filename).unwrap().as_slice());
    log!(cpu, "NES ROM Loaded",);
    cpu.reset();
    //cpu.set_pc(0xC000); // TODO: Remove hack used for nestest
}

pub fn log_window(ctx: &egui::Context, title: &'static str, log: &Rc<RefCell<String>>) {
    egui::Window::new(title)
        .vscroll(true)
        .hscroll(true)
        .default_size((300.0, 200.0))
        .show(ctx, |ui| {
            egui::ScrollArea::new([true, true]).show(ui, |ui| {
                let log = log.borrow_mut();
                let mut lines = log.lines().rev().take(20).collect::<Vec<&str>>();
                lines.reverse();
                //ui.add(egui::Label::new(lines.join("\n")).code().wrap(false));
                ui.code(lines.join("\n"));
            })
        });
}

pub fn dump_window(ctx: &egui::Context, title: &'static str, text: &Option<String>) -> bool {
    let mut result = false;

    if let Some(text) = text {
        egui::Window::new(title)
            .vscroll(true)
            .hscroll(true)
            .default_size((300.0, 200.0))
            .show(ctx, |ui| {
                egui::ScrollArea::new([true, true]).show(ui, |ui| {
                    if ui.add(egui::Button::new("Close")).clicked() {
                        result = true;
                    }
                    ui.code(text);
                })
            });
    }

    result
}

pub fn handle_input(joypad: &mut Joypad, input_state: &egui::InputState) {
    joypad.state.down = input_state.key_down(egui::Key::ArrowDown);
    joypad.state.up = input_state.key_down(egui::Key::ArrowUp);
    joypad.state.left = input_state.key_down(egui::Key::ArrowLeft);
    joypad.state.right = input_state.key_down(egui::Key::ArrowRight);
    joypad.state.select = input_state.key_down(egui::Key::Space);
    joypad.state.start = input_state.key_down(egui::Key::Enter);
    joypad.state.a = input_state.key_down(egui::Key::A);
    joypad.state.b = input_state.key_down(egui::Key::S);
}

pub struct NESEmulator {
    cpu: CPU,
    log: Rc<RefCell<String>>,
    instruction_log: Rc<RefCell<String>>,
    loader: LoaderSelection,
    filename: String,
    running: bool,
    speed: usize,
    scale: f32,
    pos_x: f64,
    pos_y: f64,
    rotation: f64,
    num_instr: usize,
    memdump: Option<String>,
    ppu_memdump: Option<String>,
    disasm: Option<String>,
    ppu_window: PPUWindow,
    image: egui::ColorImage,
    texture: Option<egui::TextureHandle>,
}

impl NESEmulator {
    pub fn new(cc: &eframe::CreationContext<'_>) -> Self {
        let mut cpu = CPU::new();
        let log = Rc::new(RefCell::new(String::new()));
        let instruction_log = Rc::new(RefCell::new(String::new()));

        cpu.set_log_hook(get_log_hook(log.clone()));
        cpu.set_instruction_log_hook(get_instruction_log_hook(instruction_log.clone()));

        let loader = LoaderSelection::NES;
        let filename = String::from("nestest.nes");
        let running = false;
        let speed = 1;

        let scale = 2.;
        let pos_x = 0.;
        let pos_y = 0.;
        let rotation = 0.;

        let num_instr = 1;

        let memdump = None;
        let disasm = None;
        let ppu_memdump = None;

        let ppu_window = PPUWindow::new();

        let image = egui::ColorImage::new([256, 240], egui::Color32::TRANSPARENT);
        let texture: Option<egui::TextureHandle> = None;

        Self {
            cpu,
            log,
            instruction_log,
            loader,
            filename,
            running,
            speed,
            scale,
            pos_x,
            pos_y,
            rotation,
            num_instr,
            memdump,
            ppu_memdump,
            disasm,
            ppu_window,
            image,
            texture,
        }
    }

    fn update_image(&mut self) {
        if let Some(ppu) = self.cpu.bus.get_ppu_mut() {
            let buffer = self.ppu_window.draw(ppu);
            self.image = egui::ColorImage {
                size: self.image.size,
                pixels: buffer.to_vec(),
            };
        }
    }
}

impl eframe::App for NESEmulator {
    fn update(&mut self, egui_ctx: &egui::Context, frame: &mut eframe::Frame) {
        egui::Window::new("Display").show(egui_ctx, |ui| {
            ui.horizontal(|ui| {
                ui.label("Scale");
                ui.add(egui::DragValue::new(&mut self.scale).speed(0.01));
                ui.label("Rotation");
                ui.add(egui::DragValue::new(&mut self.rotation).speed(0.01));
            });

            ui.horizontal(|ui| {
                ui.label("X");
                ui.add(egui::DragValue::new(&mut self.pos_x).speed(0.5));
                ui.label("Y");
                ui.add(egui::DragValue::new(&mut self.pos_y).speed(0.5));
            });
        });

        egui::Window::new("Inspect").show(egui_ctx, |ui| {
            if ui.add(egui::Button::new("Dump memory")).clicked() {
                let bytes = self.cpu.dump_mem(0, 0xFFFF);

                let mut counter = 0;
                let mut string = String::from("0x0000: ");

                let mut line_counter = 0x0000;

                for item in bytes {
                    if counter == 16 {
                        line_counter += 0x0010;
                        string += &format!("\n{:#06x}: ", line_counter);
                        counter = 0;
                    }

                    string += &format!("{:02x} ", item);
                    counter += 1;
                }

                self.memdump = Some(string);
                log!(self.cpu, "Memory dumped",);
            }

            if let Some(ppu) = self.cpu.bus.get_ppu_mut() {
                if ui.add(egui::Button::new("Dump PPU memory")).clicked() {
                    let bytes = ppu.dump_mem(0, 0xFFFF);

                    let mut counter = 0;
                    let mut string = String::from("0x0000: ");

                    let mut line_counter = 0x0000;

                    for item in bytes {
                        if counter == 16 {
                            line_counter += 0x0010;
                            string += &format!("\n{:#06x}: ", line_counter);
                            counter = 0;
                        }

                        string += &format!("{:02x} ", item);
                        counter += 1;
                    }

                    self.ppu_memdump = Some(string);
                    log!(self.cpu, "PPU Memory dumped",);
                }
            }

            if ui
                .add(egui::Button::new("Disassemble instructions"))
                .clicked()
            {
                let instructions = self.cpu.disassemble(0, 0xFFFF);

                let text = instructions
                    .iter()
                    .map(|instr| format!("{:#06x}:\t{:?}\n", instr.0, instr.1))
                    .fold(String::new(), |acc, t| acc + &t[..]);

                self.disasm = Some(text);
                log!(self.cpu, "Memory disassembled",);
            }
        });

        egui::Window::new("Control").show(egui_ctx, |ui| {
            if self.running {
                if ui.add(egui::Button::new("Stop")).clicked() {
                    self.running = false
                }
            } else {
                ui.horizontal(|ui| {
                    ui.label("Speed (cycles per frame)");
                    ui.add(egui::DragValue::new(&mut self.speed).speed(0.1));
                });

                if ui.add(egui::Button::new("Start")).clicked() {
                    self.running = true
                }

                if ui.add(egui::Button::new("Run cycle")).clicked() {
                    self.cpu.run_cycle();
                }

                if ui.add(egui::Button::new("Run instruction")).clicked() {
                    self.cpu.run_next();
                }

                ui.horizontal(|ui| {
                    ui.label("Run n instructions");
                    ui.add(egui::DragValue::new(&mut self.num_instr).speed(0.1));
                    if ui.add(egui::Button::new("Go")).clicked() {
                        for _ in 0..self.num_instr {
                            self.cpu.run_next();
                        }
                    }
                });
            }
        });

        egui::Window::new("Load ROM").show(egui_ctx, |ui| {
            ui.selectable_value(&mut self.loader, LoaderSelection::NES, "NES");
            ui.selectable_value(&mut self.loader, LoaderSelection::MOS6502, "MOS6502");
            ui.add(egui::TextEdit::singleline(&mut self.filename));

            if ui.add(egui::Button::new("Load")).clicked() {
                let mut path = std::path::PathBuf::new();
                path.push(&self.filename);

                match self.loader {
                    LoaderSelection::MOS6502 => load_6502_rom(&mut self.cpu, &path),
                    LoaderSelection::NES => load_nes_rom(&mut self.cpu, &path),
                }

                //self.cpu.pc = 0xc000;
            }
        });

        egui::Window::new("Framebuffer").show(egui_ctx, |ui| {
            if let Some(texture) = &self.texture {
                ui.image(
                    texture,
                    egui::vec2(
                        texture.size_vec2().x * self.scale,
                        texture.size_vec2().y * self.scale,
                    ),
                );
            }
        });

        log_window(egui_ctx, "Log", &self.log);
        log_window(egui_ctx, "Instruction log", &self.instruction_log);
        if dump_window(egui_ctx, "Memory dump", &self.memdump) {
            self.memdump = None
        };
        if dump_window(egui_ctx, "PPU Memory dump", &self.ppu_memdump) {
            self.ppu_memdump = None
        };
        if dump_window(egui_ctx, "Disassembled memory", &self.disasm) {
            self.disasm = None
        };

        if self.running {
            for _ in 0..self.speed {
                if let Some(joypad) = self.cpu.bus.get_joypad_mut() {
                    handle_input(joypad, &egui_ctx.input());
                }
                self.cpu.run_cycle();
            }

            self.update_image();
            self.texture = Some(egui_ctx.load_texture("render", self.image.clone()));

            egui_ctx.request_repaint();
        }
    }
}
