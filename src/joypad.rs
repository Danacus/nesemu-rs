use crate::data_types::*;

#[derive(Debug, Default)]
pub struct JoypadState {
    pub a: bool,
    pub b: bool,
    pub select: bool,
    pub start: bool,
    pub up: bool,
    pub down: bool,
    pub left: bool,
    pub right: bool,
}

impl JoypadState {
    pub fn get(&self, index: u8) -> bool {
        match index {
            7 => self.right,
            6 => self.left,
            5 => self.down,
            4 => self.up,
            3 => self.start,
            2 => self.select,
            1 => self.b,
            0 => self.a,
            _ => false,
        }
    }

    pub fn set(&mut self, index: u8, value: bool) {
        match index {
            7 => self.right = value,
            6 => self.left = value,
            5 => self.down = value,
            4 => self.up = value,
            3 => self.start = value,
            2 => self.select = value,
            1 => self.b = value,
            0 => self.a = value,
            _ => {}
        }
    }
}

#[derive(Debug)]
pub struct Joypad {
    strobe: bool,
    index: u8,
    pub state: JoypadState,
}

impl Joypad {
    pub fn new() -> Self {
        Self {
            strobe: false,
            index: 0,
            state: JoypadState::default(),
        }
    }
}

impl Memory for Joypad {
    fn read(&mut self, _address: Address16) -> Option<Value> {
        if self.index > 7 {
            return Some(1);
        }

        let response = if self.state.get(self.index) { 1 } else { 0 };

        if !self.strobe && self.index <= 7 {
            self.index += 1;
        }

        Some(response)
    }

    fn inspect(&mut self, _address: Address16) -> Option<Value> {
        if self.index > 7 {
            return Some(1);
        }

        let response = if self.state.get(self.index) { 1 } else { 0 };

        Some(response)
    }

    fn write(&mut self, _address: Address16, data: Value) {
        self.strobe = data & 1 == 1;
        if self.strobe {
            self.index = 0;
        }
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_strobe_mode() {
        let mut joypad = Joypad::new();
        joypad.write(0, 1);
        joypad.state.a = true;

        for _x in 0..10 {
            assert_eq!(joypad.read(0).unwrap(), 1);
        }
    }

    #[test]
    fn test_strobe_mode_on_off() {
        let mut joypad = Joypad::new();

        joypad.write(0, 0);
        joypad.state.right = true;
        joypad.state.left = true;
        joypad.state.select = true;
        joypad.state.b = true;

        for _ in 0..=1 {
            assert_eq!(joypad.read(0).unwrap(), 0);
            assert_eq!(joypad.read(0).unwrap(), 1);
            assert_eq!(joypad.read(0).unwrap(), 1);
            assert_eq!(joypad.read(0).unwrap(), 0);
            assert_eq!(joypad.read(0).unwrap(), 0);
            assert_eq!(joypad.read(0).unwrap(), 0);
            assert_eq!(joypad.read(0).unwrap(), 1);
            assert_eq!(joypad.read(0).unwrap(), 1);

            for _x in 0..10 {
                assert_eq!(joypad.read(0).unwrap(), 1);
            }

            joypad.write(0, 1);
            joypad.write(0, 0);
        }
    }
}
