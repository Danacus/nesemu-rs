use crate::apu::APU;
use crate::cpu::CPU;
use crate::data_types::*;
use crate::joypad::Joypad;
use crate::ppu::Mirroring;
use crate::ppu::PPU;

pub struct MOS6502Mapper {
    ram: RAM,
}

impl MOS6502Mapper {
    pub fn new() -> Box<Self> {
        Box::new(Self {
            ram: RAM::new(0x10000),
        })
    }
}

impl Memory for MOS6502Mapper {
    fn read(&mut self, address: Address16) -> Option<Value> {
        self.ram.read(address)
    }

    fn write(&mut self, address: Address16, data: Value) {
        self.ram.write(address, data)
    }
}

pub struct NESMapper {
    ram: RAM,
    ppu: PPU,
    apu: APU,
    joypad: Joypad,
}

impl NESMapper {
    pub fn new(mirroring: Mirroring, chr_rom: &[u8]) -> Box<Self> {
        Box::new(Self {
            ram: RAM::new(0x0800),
            ppu: PPU::new(mirroring, chr_rom),
            apu: APU {},
            joypad: Joypad::new(),
        })
    }

    fn ppu_oam_dma_copy(&mut self, high_byte: Value) {
        let start = (high_byte as u16) << 8;

        for i in start..(start + 0xFF) {
            let data = self.ram.read(i);
            self.ppu.write_oam((i - start) as u8, data.unwrap_or(0));
        }
    }
}

impl Memory for NESMapper {
    fn read(&mut self, address: Address16) -> Option<Value> {
        match address {
            0x0000..=0x1FFF => self.ram.read(address % 0x0800),
            0x2000..=0x3FFF => self.ppu.read(0x2000 + address % 8),
            0x4016 => self.joypad.read(address),
            0x4017 => Some(0),
            _ => None,
        }
    }

    fn inspect(&mut self, address: Address16) -> Option<Value> {
        match address {
            0x0000..=0x1FFF => self.ram.inspect(address % 0x0800),
            0x2000..=0x3FFF => self.ppu.inspect(0x2000 + address % 8),
            0x4016 => self.joypad.inspect(address),
            0x4017 => Some(0),
            _ => None,
        }
    }

    fn write(&mut self, address: Address16, data: Value) {
        match address {
            0x0000..=0x1FFF => self.ram.write(address % 0x0800, data),
            0x2000..=0x3FFF => self.ppu.write(0x2000 + address % 8, data),
            0x4014 => self.ppu_oam_dma_copy(data),
            0x4016 => self.joypad.write(address, data),
            _ => {}
        }
    }

    fn get_ppu_mut(&mut self) -> Option<&mut PPU> {
        Some(&mut self.ppu)
    }

    fn get_apu_mut(&mut self) -> Option<&mut APU> {
        Some(&mut self.apu)
    }

    fn get_joypad_mut(&mut self) -> Option<&mut Joypad> {
        Some(&mut self.joypad)
    }
}

pub struct Mapper0 {
    prg_ram: RAM,
    prg_rom: ROM,
}

impl Mapper0 {
    pub fn new(prg_rom: &[u8]) -> Box<Self> {
        Box::new(Self {
            prg_ram: RAM::new(0x2000),
            prg_rom: ROM::new(prg_rom),
        })
    }
}

impl Memory for Mapper0 {
    fn read(&mut self, address: Address16) -> Option<Value> {
        match address {
            0x6000..=0x7FFF => self.prg_ram.read(address - 0x6000),
            0x8000..=0xBFFF => self.prg_rom.read(address - 0x8000),
            0xC000..=0xFFFF => self.prg_rom.read(address - 0xC000),
            _ => None,
        }
    }

    fn write(&mut self, address: Address16, data: Value) {
        match address {
            0x6000..=0x7FFF => self.prg_ram.write(address - 0x6000, data),
            0x8000..=0xBFFF => self.prg_rom.write(address - 0x8000, data),
            0xC000..=0xFFFF => self.prg_rom.write(address - 0xC000, data),
            _ => {}
        }
    }
}
