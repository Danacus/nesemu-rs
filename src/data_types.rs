use crate::apu::APU;
use crate::joypad::Joypad;
use crate::ppu::PPU;

pub type Address8 = u8;
pub type Address16 = u16;
pub type RelativeAddress8 = i8;
pub type Value = u8;

pub trait Memory {
    fn write(&mut self, address: Address16, data: Value);
    fn read(&mut self, address: Address16) -> Option<Value>;

    fn inspect(&mut self, address: Address16) -> Option<Value> {
        self.read(address)
    }

    fn get_ppu_mut(&mut self) -> Option<&mut PPU> {
        None
    }
    fn get_apu_mut(&mut self) -> Option<&mut APU> {
        None
    }
    fn get_joypad_mut(&mut self) -> Option<&mut Joypad> {
        None
    }
}

pub struct Bus {
    pub mappers: Vec<Box<dyn Memory>>,
    pub null: NullROM,
    pub cycles: usize,
}

impl Bus {
    pub fn new() -> Self {
        Self {
            mappers: Vec::new(),
            null: NullROM::new(),
            cycles: 0,
        }
    }

    pub fn add_mapper(&mut self, mapper: Box<dyn Memory>) {
        self.mappers.push(mapper);
    }

    pub fn tick(&mut self, cycles: u8) {
        self.cycles += cycles as usize;
        self.get_ppu_mut().map(|ppu| ppu.tick(3 * cycles));
    }
}

impl Memory for Bus {
    fn read(&mut self, address: Address16) -> Option<Value> {
        self.mappers
            .iter_mut()
            .filter_map(|m| m.read(address))
            .next()
    }

    fn inspect(&mut self, address: Address16) -> Option<Value> {
        self.mappers
            .iter_mut()
            .filter_map(|m| m.inspect(address))
            .next()
    }

    fn write(&mut self, address: Address16, data: Value) {
        self.mappers.iter_mut().for_each(|m| m.write(address, data))
    }

    fn get_ppu_mut(&mut self) -> Option<&mut PPU> {
        self.mappers
            .iter_mut()
            .filter_map(|m| m.get_ppu_mut())
            .next()
    }

    fn get_apu_mut(&mut self) -> Option<&mut APU> {
        self.mappers
            .iter_mut()
            .filter_map(|m| m.get_apu_mut())
            .next()
    }

    fn get_joypad_mut(&mut self) -> Option<&mut Joypad> {
        self.mappers
            .iter_mut()
            .filter_map(|m| m.get_joypad_mut())
            .next()
    }
}

pub struct RAM {
    data: Vec<u8>,
}

impl RAM {
    pub fn new(size: usize) -> Self {
        Self {
            data: vec![0; size],
        }
    }
}

impl Memory for RAM {
    fn read(&mut self, address: Address16) -> Option<Value> {
        self.data.get(address as usize).cloned()
    }

    fn write(&mut self, address: Address16, data: Value) {
        self.data[address as usize] = data;
    }
}

pub struct ROM {
    data: Vec<u8>,
}

impl ROM {
    pub fn new(data: &[u8]) -> Self {
        Self { data: data.into() }
    }
}

impl Memory for ROM {
    fn read(&mut self, address: Address16) -> Option<Value> {
        self.data.get(address as usize).cloned()
    }

    fn write(&mut self, _: Address16, _: Value) {
        panic!("Attempt to write read-only memory");
    }
}

pub struct NullROM;

impl NullROM {
    pub fn new() -> Self {
        Self {}
    }
}

impl Memory for NullROM {
    fn read(&mut self, _: Address16) -> Option<Value> {
        Some(0x0)
    }

    fn write(&mut self, _: Address16, _: Value) {
        panic!("Attempt to write read-only memory");
    }
}
