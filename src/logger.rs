pub type LogHook = Box<dyn Fn(&str)>;

pub struct Logger {
    hook: LogHook,
}

impl Logger {
    pub fn new(hook: LogHook) -> Self {
        Self {
            hook,
        }
    }

    pub fn default() -> Self {
        Self {
            hook: Box::new(|message| println!("{}", message)),
        }
    }

    pub fn log(&self, message: &str) {
        (*self.hook)(message);
    }

    pub fn set_hook(&mut self, hook: LogHook) {
        self.hook = hook;
    }
}

pub trait Log {
    fn get_logger(&self) -> &Logger;
    fn get_logger_mut(&mut self) -> &mut Logger;

    fn log(&self, message: &str) {
        self.get_logger().log(message); 
    }

    fn set_hook(&mut self, hook: LogHook) {
        self.get_logger_mut().set_hook(hook);
    }
}

