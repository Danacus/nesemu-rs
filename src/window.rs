use crate::data_types::*;
use crate::ppu::PPU;
use eframe::egui::Color32 as Color;

pub const WIDTH: u32 = 256;
pub const HEIGHT: u32 = 240;

lazy_static! {
    pub static ref SYSTEM_PALLETE: [(u8, u8, u8); 64] = [
        (0x80, 0x80, 0x80),
        (0x00, 0x3D, 0xA6),
        (0x00, 0x12, 0xB0),
        (0x44, 0x00, 0x96),
        (0xA1, 0x00, 0x5E),
        (0xC7, 0x00, 0x28),
        (0xBA, 0x06, 0x00),
        (0x8C, 0x17, 0x00),
        (0x5C, 0x2F, 0x00),
        (0x10, 0x45, 0x00),
        (0x05, 0x4A, 0x00),
        (0x00, 0x47, 0x2E),
        (0x00, 0x41, 0x66),
        (0x00, 0x00, 0x00),
        (0x05, 0x05, 0x05),
        (0x05, 0x05, 0x05),
        (0xC7, 0xC7, 0xC7),
        (0x00, 0x77, 0xFF),
        (0x21, 0x55, 0xFF),
        (0x82, 0x37, 0xFA),
        (0xEB, 0x2F, 0xB5),
        (0xFF, 0x29, 0x50),
        (0xFF, 0x22, 0x00),
        (0xD6, 0x32, 0x00),
        (0xC4, 0x62, 0x00),
        (0x35, 0x80, 0x00),
        (0x05, 0x8F, 0x00),
        (0x00, 0x8A, 0x55),
        (0x00, 0x99, 0xCC),
        (0x21, 0x21, 0x21),
        (0x09, 0x09, 0x09),
        (0x09, 0x09, 0x09),
        (0xFF, 0xFF, 0xFF),
        (0x0F, 0xD7, 0xFF),
        (0x69, 0xA2, 0xFF),
        (0xD4, 0x80, 0xFF),
        (0xFF, 0x45, 0xF3),
        (0xFF, 0x61, 0x8B),
        (0xFF, 0x88, 0x33),
        (0xFF, 0x9C, 0x12),
        (0xFA, 0xBC, 0x20),
        (0x9F, 0xE3, 0x0E),
        (0x2B, 0xF0, 0x35),
        (0x0C, 0xF0, 0xA4),
        (0x05, 0xFB, 0xFF),
        (0x5E, 0x5E, 0x5E),
        (0x0D, 0x0D, 0x0D),
        (0x0D, 0x0D, 0x0D),
        (0xFF, 0xFF, 0xFF),
        (0xA6, 0xFC, 0xFF),
        (0xB3, 0xEC, 0xFF),
        (0xDA, 0xAB, 0xEB),
        (0xFF, 0xA8, 0xF9),
        (0xFF, 0xAB, 0xB3),
        (0xFF, 0xD2, 0xB0),
        (0xFF, 0xEF, 0xA6),
        (0xFF, 0xF7, 0x9C),
        (0xD7, 0xE8, 0x95),
        (0xA6, 0xED, 0xAF),
        (0xA2, 0xF2, 0xDA),
        (0x99, 0xFF, 0xFC),
        (0xDD, 0xDD, 0xDD),
        (0x11, 0x11, 0x11),
        (0x11, 0x11, 0x11)
    ];
    pub static ref COLOR_MAP: Vec<Color> = SYSTEM_PALLETE
        .iter()
        .map(|(r, g, b)| Color::from_rgb(*r, *g, *b))
        .collect();
}

pub struct Frame {
    pub buffer: Vec<Color>,
}

impl Frame {
    pub fn new() -> Self {
        Self {
            buffer: vec![Color::default(); (WIDTH * HEIGHT) as usize],
        }
    }

    pub fn set_pixel(&mut self, x: usize, y: usize, color: Color) {
        if y * WIDTH as usize + x < (WIDTH * HEIGHT) as usize {
            self.buffer[y * WIDTH as usize + x] = color;
        }
    }
}

pub struct PPUWindow {
    frame: Frame,
}

impl PPUWindow {
    pub fn new() -> Self {
        //COLOR_MAP.iter().for_each(|c| println!("{:?}", c));

        Self {
            frame: Frame::new(),
        }
    }

    pub fn get_color(&self, index: usize) -> Color {
        COLOR_MAP[index]
    }

    pub fn draw_tile(
        &mut self,
        ppu: &mut PPU,
        bank: usize,
        tile_n: usize,
        offset_x: usize,
        offset_y: usize,
    ) {
        let bank = (bank * 0x1000) as usize;
        let tile = ((bank + tile_n * 16)..=(bank + tile_n * 16 + 15))
            .map(|i| ppu.mapper.read(i as u16))
            .collect::<Vec<_>>();

        for y in 0..=7 {
            let mut upper = tile[y].unwrap_or(0);
            let mut lower = tile[y + 8].unwrap_or(0);

            for x in (0..=7).rev() {
                let value = (1 & upper) << 1 | (1 & lower);
                upper = upper >> 1;
                lower = lower >> 1;
                //println!("{}", value);
                let rgb = match value {
                    0 => COLOR_MAP[0x01],
                    1 => COLOR_MAP[0x23],
                    2 => COLOR_MAP[0x27],
                    3 => COLOR_MAP[0x30],
                    _ => panic!("can't be"),
                };
                //println!("{:#?}", rgb);
                self.frame.set_pixel(x + offset_x, y + offset_y, rgb)
            }
        }
    }

    pub fn draw_background(&mut self, mut ppu: &mut PPU) {
        let bank = ppu.ppu_ctrl.background_table;

        for i in 0..0x03c0 {
            let tile = ppu.mapper.read(0x2000 + i as u16).unwrap_or(0);
            let tile_x = i % 32;
            let tile_y = i / 32;

            self.draw_tile(
                &mut ppu,
                if bank { 1 } else { 0 },
                tile as usize,
                tile_x * 8,
                tile_y * 8,
            );
        }
    }

    pub fn draw(&mut self, mut ppu: &mut PPU) -> &Vec<Color> {
        /*
        for i in 0..256 {
            let x = (i as u32 * 8).rem_euclid(WIDTH);
            let y = ((i * 8) / WIDTH) * 8;
            self.draw_tile(&mut ppu, 0, i as usize, x as usize, y as usize);
        }

        for i in 0..256 {
            let x = (i as u32 * 8).rem_euclid(WIDTH);
            let y = ((i * 8) / WIDTH + 8) * 8;
            self.draw_tile(&mut ppu, 1, i as usize, x as usize, y as usize);
        }
        */
        self.draw_background(ppu);
        &self.frame.buffer
    }
}
