use crate::data_types::*;
use crate::instruction::*;

use pad::PadStr;

pub enum Status {
    Carry,
    Zero,
    InterruptDisable,
    Decimal,
    Break,
    Reserve,
    Overflow,
    Negative,
}

impl Status {
    fn get_index(&self) -> usize {
        match self {
            Status::Carry => 0,
            Status::Zero => 1,
            Status::InterruptDisable => 2,
            Status::Decimal => 3,
            Status::Break => 4,
            Status::Reserve => 5,
            Status::Overflow => 6,
            Status::Negative => 7,
        }
    }
}

#[macro_export]
macro_rules! log {
    ($cpu:expr, $pat:expr, $($var:expr),* $(,)?) => {
        (*$cpu.log_hook)(format!($pat, $($var),*))
    };
}

/**
 * 6502 CPU Emulator
 */
pub struct CPU {
    // Registers
    pub accumulator: u8,
    pub index_x: u8,
    pub index_y: u8,
    pub pc: u16,
    pub status: [bool; 8],
    pub bus: Bus,

    // Stack
    pub sp: u8,

    // Other
    cycle_counter: u8,
    pub log_hook: Box<dyn Fn(String)>,
    pub instruction_log_hook: Box<dyn FnMut(String)>,
    page_cross: bool,
}

impl CPU {
    pub fn new() -> Self {
        let bus = Bus::new();

        Self {
            accumulator: 0,
            index_x: 0,
            index_y: 0,
            pc: 0,
            status: [false, false, true, false, true, true, false, false],
            bus,
            sp: 0xFD,
            cycle_counter: 0,
            log_hook: Box::new(|message| println!("{}", message)),
            instruction_log_hook: Box::new(|message| println!("{}", message)),
            page_cross: false,
        }
    }

    pub fn set_log_hook(&mut self, fun: Box<dyn Fn(String)>) {
        self.log_hook = fun;
    }

    pub fn set_instruction_log_hook(&mut self, fun: Box<dyn FnMut(String)>) {
        self.instruction_log_hook = fun;
    }

    fn stack_push(&mut self, value: Value) {
        self.write_mem(0x100 + self.sp as u16, value);
        self.sp = self.sp.overflowing_sub(1).0;
    }

    fn stack_pop(&mut self) -> Value {
        self.sp = self.sp.overflowing_add(1).0;
        self.read_mem(0x100 + self.sp as u16)
    }

    fn stack_push_address(&mut self, addr: Address16) {
        let bytes = addr.to_be_bytes();
        self.write_mem(0x100 + self.sp as u16, bytes[0]);
        self.write_mem(0x100 + self.sp as u16 - 1, bytes[1]);
        self.sp = self.sp.overflowing_sub(2).0;
    }

    fn stack_pop_address(&mut self) -> Address16 {
        let lo = self.stack_pop();
        let hi = self.stack_pop();
        Address16::from_le_bytes([lo, hi])
    }

    fn write_mem(&mut self, address: Address16, data: Value) {
        self.bus.write(address, data);
    }

    fn read_mem(&mut self, address: Address16) -> Value {
        self.bus.read(address).unwrap_or_else(|| {
            //log!(self, "Invalid bus read: {}", address);
            0
        })
    }

    fn inspect_mem(&mut self, address: Address16) -> Value {
        self.bus.inspect(address).unwrap_or_else(|| {
            //log!(self, "Invalid bus read: {}", address);
            0
        })
    }

    pub fn dump_mem(&mut self, start: Address16, end: Address16) -> Vec<u8> {
        let mut res = Vec::new();

        for i in start..=end {
            res.push(self.read_mem(i));
        }

        res
    }

    fn get_status(&self, status: Status) -> bool {
        self.status[status.get_index()]
    }

    fn set_status(&mut self, status: Status, value: bool) {
        self.status[status.get_index()] = value;
    }

    pub fn get_status_byte(&self) -> Value {
        let mut status = 0;

        for i in 0..8 {
            if self.status[i] {
                status += 1 << i;
            }
        }

        status
    }

    fn set_status_byte(&mut self, status: Value, ignore_unused: bool) {
        for i in 0..8 {
            if ignore_unused && (i == 4 || i == 5) {
                continue;
            }

            self.status[i] = status & (1 << i) != 0;
        }
    }

    fn address_page_wrap_add(&self, address: Address16, value: Value) -> Address16 {
        (address & 0xFF00) + (address as Value).wrapping_add(value) as u16
    }

    fn addr_page(&self, address: Address16) -> u8 {
        ((address & 0xFF00) >> 8) as u8
    }

    fn address(&mut self, addressing_mode: AddressingMode) -> Address16 {
        self.page_cross = false;

        use AddressingMode::*;

        match addressing_mode {
            Implicit => panic!("Cannot get address implicit addressing mode"),
            Accumulator => panic!("Cannot get address accumulator addressing mode"),
            Immediate(_) => panic!("Cannot get address immediate addressing mode"),
            ZeroPage(addr) => addr as u16,
            ZeroPageX(addr) => (addr as u16 + self.index_x as u16) % 256,
            ZeroPageY(addr) => (addr as u16 + self.index_y as u16) % 256,
            Absolute(addr) => addr,
            AbsoluteX(addr) => {
                let (res, overflow) = addr.overflowing_add(self.index_x as u16);
                self.page_cross = overflow || self.addr_page(res) != self.addr_page(addr);
                res
            }
            AbsoluteY(addr) => {
                let (res, overflow) = addr.overflowing_add(self.index_y as u16);
                self.page_cross = overflow || self.addr_page(res) != self.addr_page(addr);
                res
            }
            Indirect(addr) => {
                self.read_mem(addr) as u16
                    + ((self.read_mem(self.address_page_wrap_add(addr, 1)) as u16) << 8)
            }
            IndirectX(addr) => {
                self.read_mem((addr as u16 + self.index_x as u16) % 256) as u16
                    + ((self.read_mem((addr as u16 + self.index_x as u16 + 1) % 256) as u16) << 8)
            }
            IndirectY(addr) => {
                let (res, overflow) = (self.read_mem(addr as u16) as u16
                    + ((self.read_mem(((addr as u16) + 1) % 256) as u16) << 8))
                    .overflowing_add(self.index_y as u16);
                self.page_cross = overflow || ((addr as u16) + 1) >= 256;
                res
            }
            Relative(addr) => (self.pc as i16 + addr as i16) as u16,
        }
    }

    fn read_address(&mut self, addressing_mode: AddressingMode) -> Value {
        self.read_address_inner(addressing_mode, false)
    }

    fn inspect_address(&mut self, addressing_mode: AddressingMode) -> Value {
        self.read_address_inner(addressing_mode, true)
    }

    fn read_address_inner(&mut self, addressing_mode: AddressingMode, inspect: bool) -> Value {
        use AddressingMode::*;

        match addressing_mode {
            Implicit => panic!("Cannot read in implicit addressing mode"),
            Accumulator => self.accumulator,
            Immediate(value) => value,
            _ => {
                let addr = self.address(addressing_mode);

                if inspect {
                    self.inspect_mem(addr)
                } else {
                    self.read_mem(addr)
                }
            }
        }
    }

    fn write_address(&mut self, addressing_mode: AddressingMode, value: Value) {
        use AddressingMode::*;

        match addressing_mode {
            Implicit => panic!("Canot write in implicit addressing mode"),
            Accumulator => self.accumulator = value,
            Immediate(_) => panic!("Cannot write in immediate mode"),
            _ => {
                let addr = self.address(addressing_mode);
                self.write_mem(addr, value);
            }
        }
    }

    pub fn load_data(&mut self, position: Address16, data: &[Value]) {
        for (i, value) in data.iter().enumerate() {
            if i >= 0xFFFF {
                log!(self, "Warning: unable to load all data",);
                break;
            }
            self.write_mem(position + i as u16, *value);
        }
    }

    pub fn set_pc(&mut self, addr: Address16) {
        self.pc = addr;
    }

    pub fn run_cycle(&mut self) -> bool {
        if Some(true) == self.bus.get_ppu_mut().map(|p| p.poll_nmi()) {
            self.nmi();
        }

        let mut result = true;

        if self.cycle_counter == 0 {
            result = self.run_next();
        }

        self.cycle_counter -= 1;

        result
    }

    pub fn reset(&mut self) {
        self.pc = self.address(AddressingMode::Indirect(0xfffc));
        self.accumulator = 0;
        self.index_x = 0;
        self.index_y = 0;
        self.sp = 0xfd;
        self.set_status_byte(0b00100100, false);
        self.cycle_counter = 7;
        self.bus.tick(self.cycle_counter);
    }

    pub fn irq(&mut self) {
        if self.get_status(Status::InterruptDisable) {
            return;
        }

        self.stack_push_address(self.pc);
        self.stack_push(self.get_status_byte());
        self.pc = self.address(AddressingMode::Indirect(0xfffe));
        self.cycle_counter = 7;
        self.bus.tick(self.cycle_counter);
    }

    pub fn nmi(&mut self) {
        self.stack_push_address(self.pc);
        self.stack_push(self.get_status_byte());
        self.pc = self.address(AddressingMode::Indirect(0xfffa));
        self.cycle_counter = 8;
        self.bus.tick(self.cycle_counter);
    }

    pub fn disassemble(
        &mut self,
        start: Address16,
        end: Address16,
    ) -> Vec<(Address16, Instruction)> {
        let mut current = start as usize;
        let mut res = Vec::new();

        while current <= end as usize {
            let instr = self.get_instruction(current as u16);
            res.push((current as u16, instr));
            current += instr.length() as usize;
        }

        res
    }

    pub fn get_instruction(&mut self, pos: Address16) -> Instruction {
        let mut instr_buf = Vec::new();

        for i in 0..=3 {
            if pos as usize + i <= 0xffff {
                instr_buf.push(self.read_mem(pos + i as u16));
            } else {
                return Instruction::new(OpCode::NOP, AddressingMode::Implicit, 1.into());
            }
        }

        (&instr_buf).into()
    }

    pub fn format_opcode_byte(&self, byte: Option<u8>) -> String {
        byte.map(|b| format!("{:02X}", b)).unwrap_or("  ".into())
    }

    pub fn log_instruction(&mut self, mut instruction: Instruction) -> String {
        self.bus.get_ppu_mut().map(|ppu| ppu.dirty_read = true);
        let pc = format!("{:04X}", self.pc).pad_to_width(6);
        let opcode = instruction
            .op_code_raw
            .iter()
            .take(instruction.mode.length() as usize)
            .map(|b| self.format_opcode_byte(*b))
            .collect::<Vec<String>>()
            .join(" ")
            .pad_to_width(9);

        /*
        if let AddressingMode::Relative(_) = instruction.mode {
            // Hack to match nestest log
            instruction.mode = AddressingMode::Absolute(self.address(instruction.mode) + 2);
        }
        */

        let (mem_addr, stored_value) = match instruction.mode {
            AddressingMode::Immediate(_)
            | AddressingMode::Implicit
            | AddressingMode::Accumulator => (0, 0),
            _ => {
                // TODO: address here should also use inspect
                let addr = self.address(instruction.mode);
                (addr, self.inspect_address(instruction.mode))
            }
        };

        let mut instr = format!("{:?} {:?}", instruction.op_code, instruction.mode);

        match instruction.mode {
            AddressingMode::ZeroPage(_) => instr.push_str(&format!(" = {:02X}", stored_value)),
            AddressingMode::Indirect(_) => instr.push_str(&format!(" = {:04X}", mem_addr)),
            AddressingMode::Absolute(_)
                if instruction.op_code != OpCode::JMP && instruction.op_code != OpCode::JSR =>
            {
                instr.push_str(&format!(" = {:02X}", stored_value))
            }
            AddressingMode::ZeroPageX(_) | AddressingMode::ZeroPageY(_) => {
                instr.push_str(&format!(" @ {:02X} = {:02X}", mem_addr, stored_value))
            }
            AddressingMode::AbsoluteX(_) | AddressingMode::AbsoluteY(_) => {
                instr.push_str(&format!(" @ {:04X} = {:02X}", mem_addr, stored_value))
            }
            AddressingMode::Relative(offset) => {
                instr = format!(
                    "{:?} ${:04X}",
                    instruction.op_code,
                    (self.pc as i16).wrapping_add(offset as i16 + 2)
                )
            }
            AddressingMode::IndirectX(address) => instr.push_str(&format!(
                " @ {:02X} = {:04X} = {:02X}",
                (address.wrapping_add(self.index_x)),
                mem_addr,
                stored_value
            )),
            AddressingMode::IndirectY(_) => instr.push_str(&format!(
                " = {:04X} @ {:04X} = {:02X}",
                (mem_addr.wrapping_sub(self.index_y as u16)),
                mem_addr,
                stored_value
            )),
            _ => {}
        }

        let instr = instr.pad_to_width(32);
        let instr = if instruction.unofficial { "*" } else { " " }.to_owned() + &instr;

        self.bus.get_ppu_mut().map(|ppu| ppu.dirty_read = false);

        let bus_cycles = self.bus.cycles;
        let cycles = if let Some(ppu) = self.bus.get_ppu_mut() {
            format!(
                "PPU:{:>3},{:>3} CYC:{}",
                ppu.scanlines, ppu.cycles, bus_cycles
            )
        } else {
            "".into()
        };

        //let instr = instruction.log(self.pc, mem_addr, stored_value).pad_to_width(32);
        let regs = format!(
            "A:{:02X} X:{:02X} Y:{:02X} P:{:02X} SP:{:02X}",
            self.accumulator,
            self.index_x,
            self.index_y,
            self.get_status_byte(),
            self.sp
        );
        format!("{}{}{}{}", pc, opcode, instr, regs)
        //(*self.instruction_log_hook)(format!("{}{}{}{}\n", pc, opcode, instr, regs));
    }

    pub fn log_cycles(&mut self) -> String {
        let bus_cycles = self.bus.cycles;
        if let Some(ppu) = self.bus.get_ppu_mut() {
            format!(
                "PPU:{:>3},{:>3} CYC:{}",
                ppu.scanlines, ppu.cycles, bus_cycles
            )
        } else {
            "".into()
        }
    }

    pub fn run_next(&mut self) -> bool {
        let mut instr_buf = Vec::new();

        for i in 0..=3 {
            instr_buf.push(self.read_mem(self.pc + i));
        }

        let instruction = (&instr_buf).into();
        self.run_instruction(instruction)
    }

    pub fn run_instruction(&mut self, mut instruction: Instruction) -> bool {
        use OpCode::*;

        // Default status behaviour when writing the given value
        // at the end of an instruction
        macro_rules! status {
            (Zero, $value:expr) => {
                self.set_status(Status::Zero, $value == 0);
            };
            (Negative, $value:expr) => {
                self.set_status(Status::Negative, $value & 1 << 7 > 0);
            };
        }

        // Read memory based on the instructions addressing mode
        macro_rules! read {
            () => {
                self.read_address(instruction.mode)
            };
        }

        // Write memory based on the instructions addressing mode
        macro_rules! write {
            ($value:expr) => {
                self.write_address(instruction.mode, $value)
            };
        }

        // Increment the process counter with the length of the current instruction
        // and set the cycle counter to the time of the current instruction
        // TODO: support extra cycles in certain addressing modes
        macro_rules! next {
            () => {
                self.pc += instruction.length();
                self.cycle_counter = instruction.time(self.page_cross);
            };
        }

        // Shared code macros
        macro_rules! load {
            ($reg:ident) => {
                let result = read!();
                status!(Zero, result);
                status!(Negative, result);
                self.$reg = result;
                next!();
            };
        }

        macro_rules! store {
            ($reg:ident) => {
                let result = self.$reg;
                write!(result);
                next!();
            };
        }

        macro_rules! branch {
            ($condition:expr) => {
                if let AddressingMode::Relative(address) = instruction.mode {
                    if $condition {
                        instruction.time += 1;
                        // Relative addressing assumes pc is already at the next instruction
                        next!();
                        self.pc = (self.pc as i16 + address as i16) as u16;
                    } else {
                        next!();
                    }
                }
            };
        }

        macro_rules! clear {
            ($status:ident) => {
                self.set_status(Status::$status, false);
                next!();
            };
        }

        macro_rules! set {
            ($status:ident) => {
                self.set_status(Status::$status, true);
                next!();
            };
        }

        macro_rules! compare {
            ($target:ident) => {
                let data = read!();
                self.set_status(Status::Carry, self.$target >= data);
                status!(Zero, self.$target.overflowing_sub(data).0);
                status!(Negative, self.$target.overflowing_sub(data).0);
                next!();
            };
        }

        macro_rules! adc {
            ($data:expr) => {
                // Casting to u16 in case of overflow!
                let sum =
                    self.accumulator as u16 + $data as u16 + self.get_status(Status::Carry) as u16;
                log!(self, "sum: {:#x}", sum);
                self.set_status(Status::Carry, sum > 0xff);
                self.set_status(
                    Status::Overflow,
                    !(self.accumulator as u16 ^ $data as u16)
                        & (self.accumulator as u16 ^ sum)
                        & 0x80
                        != 0,
                );
                status!(Zero, sum as Value);
                status!(Negative, sum as Value);
                self.write_address(AddressingMode::Accumulator, sum as Value); // The same as self.accumulator = sum, but with style
                next!();
            }
        }

        //log!(self, "{:#x?}: {:?}", self.pc, instruction);
        /*
        let instr_log = self.log_instruction(instruction);
        let cycle_log = self.log_cycles();
        (*self.instruction_log_hook)(format!("{} {}\n", instr_log, cycle_log));
        */

        match instruction.op_code {
            ADC => {
                let data = read!();
                adc!(data);
            }
            AND => {
                let result = self.accumulator & read!();
                status!(Zero, result);
                status!(Negative, result);
                self.accumulator = result;
                next!();
            }
            ASL => {
                let data = read!();
                self.set_status(Status::Carry, data & (1 << 7) != 0);
                let result = data << 1;
                status!(Zero, result);
                status!(Negative, result);
                write!(result);
                next!();
            }
            BCC => {
                branch!(!self.get_status(Status::Carry));
            }
            BCS => {
                branch!(self.get_status(Status::Carry));
            }
            BEQ => {
                branch!(self.get_status(Status::Zero));
            }
            BIT => {
                let data = read!();
                status!(Zero, data & self.accumulator);
                status!(Negative, data);
                self.set_status(Status::Overflow, data & 1 << 6 > 0);
                next!();
            }
            BMI => {
                branch!(self.get_status(Status::Negative));
            }
            BNE => {
                branch!(!self.get_status(Status::Zero));
            }
            BPL => {
                branch!(!self.get_status(Status::Negative));
            }
            BRK => {
                next!();
                return false;
            }
            BVC => {
                branch!(!self.get_status(Status::Overflow));
            }
            BVS => {
                branch!(self.get_status(Status::Overflow));
            }
            CLC => {
                clear!(Carry);
            }
            CLD => {
                clear!(Decimal);
            }
            CLI => {
                clear!(InterruptDisable);
            }
            CLV => {
                clear!(Overflow);
            }
            CMP => {
                compare!(accumulator);
            }
            CPX => {
                compare!(index_x);
            }
            CPY => {
                compare!(index_y);
            }
            DEC => {
                let result = read!().overflowing_sub(1).0;
                status!(Zero, result);
                status!(Negative, result);
                write!(result);
                next!();
            }
            DEX => {
                let result = self.index_x.overflowing_sub(1).0;
                status!(Zero, result);
                status!(Negative, result);
                self.index_x = result;
                next!();
            }
            DEY => {
                let result = self.index_y.overflowing_sub(1).0;
                status!(Zero, result);
                status!(Negative, result);
                self.index_y = result;
                next!();
            }
            EOR => {
                let result = self.accumulator ^ read!();
                status!(Zero, result);
                status!(Negative, result);
                self.accumulator = result;
                next!();
            }
            INC => {
                let result = read!().overflowing_add(1).0;
                status!(Zero, result);
                status!(Negative, result);
                write!(result);
                next!();
            }
            INX => {
                let result = self.index_x.overflowing_add(1).0;
                status!(Zero, result);
                status!(Negative, result);
                self.index_x = result;
                next!();
            }
            INY => {
                let result = self.index_y.overflowing_add(1).0;
                status!(Zero, result);
                status!(Negative, result);
                self.index_y = result;
                next!();
            }
            JMP => {
                self.pc = self.address(instruction.mode);
                self.cycle_counter = instruction.time(self.page_cross);
            }
            JSR => {
                self.stack_push_address(self.pc + 2); // Actual address would be +3, but JSR pushes +2
                self.pc = self.address(instruction.mode);
                self.cycle_counter = instruction.time(self.page_cross);
            }
            LDA => {
                load!(accumulator);
            }
            LDX => {
                load!(index_x);
            }
            LDY => {
                load!(index_y);
            }
            LSR => {
                let data = read!();
                self.set_status(Status::Carry, data & 1 != 0);
                let result = data >> 1;
                status!(Zero, result);
                status!(Negative, 0x0);
                write!(result);
                next!();
            }
            NOP => {
                next!();
            }
            ORA => {
                let result = self.accumulator | read!();
                status!(Zero, result);
                status!(Negative, result);
                self.accumulator = result;
                next!();
            }
            PHA => {
                let data = self.accumulator;
                self.stack_push(data);
                next!();
            }
            PHP => {
                let data = self.get_status_byte() | 0b00110000; // The data on the stack has break and reserve set
                self.stack_push(data);
                next!();
            }
            PLA => {
                let data = self.stack_pop();
                status!(Zero, data);
                status!(Negative, data);
                self.accumulator = data;
                next!();
            }
            PLP => {
                let data = self.stack_pop();
                self.set_status_byte(data, true);
                next!();
            }
            ROL => {
                let carry = self.get_status(Status::Carry) as u8;
                let data = read!();
                let result = (data << 1) | carry;
                self.set_status(Status::Carry, data & (1 << 7) != 0);
                status!(Zero, result);
                status!(Negative, result);
                write!(result);
                next!();
            }
            ROR => {
                let carry = self.get_status(Status::Carry) as u8;
                let data = read!();
                let result = (data >> 1) | (carry << 7);
                self.set_status(Status::Carry, data & 1 != 0);
                status!(Zero, result);
                status!(Negative, result);
                write!(result);
                next!();
            }
            RTI => {
                let status = self.stack_pop();
                self.set_status_byte(status, true);
                let pc = self.stack_pop_address();
                next!();
                self.pc = pc;
            }
            RTS => {
                let pc = self.stack_pop_address();
                self.pc = pc;
                next!();
            }
            SBC => {
                let data = !read!();
                adc!(data);
            }
            SEC => {
                set!(Carry);
            }
            SED => {
                set!(Decimal);
            }
            SEI => {
                set!(InterruptDisable);
            }
            STA => {
                store!(accumulator);
            }
            STX => {
                store!(index_x);
            }
            STY => {
                store!(index_y);
            }
            TAX => {
                let result = self.accumulator;
                status!(Zero, result);
                status!(Negative, result);
                self.index_x = result;
                next!();
            }
            TAY => {
                let result = self.accumulator;
                status!(Zero, result);
                status!(Negative, result);
                self.index_y = result;
                next!();
            }
            TSX => {
                self.index_x = self.sp;
                status!(Zero, self.index_x);
                status!(Negative, self.index_x);
                next!();
            }
            TXA => {
                let result = self.index_x;
                status!(Zero, result);
                status!(Negative, result);
                self.accumulator = result;
                next!();
            }
            TXS => {
                self.sp = self.index_x;
                next!();
            }
            TYA => {
                let result = self.index_y;
                status!(Zero, result);
                status!(Negative, result);
                self.accumulator = result;
                next!();
            }
            _ => unimplemented!(),
        }

        self.bus.tick(instruction.time(self.page_cross));

        true
    }
}

#[cfg(test)]
mod cpu_tests {
    use super::*;
    use crate::loader::{Loader, MOS6502Loader};
    use AddressingMode::*;
    use OpCode::*;

    fn new_cpu() -> CPU {
        let mut cpu = CPU::new();
        MOS6502Loader::load(&mut cpu, &[]);
        cpu.set_pc(0x0);
        cpu
    }

    #[test]
    fn status_test() {
        let cpu = new_cpu();
        assert_eq!(cpu.get_status(Status::InterruptDisable), true);
        assert_eq!(cpu.get_status_byte(), 0x34);
    }

    #[test]
    fn stack_test() {
        let mut cpu = new_cpu();

        cpu.sp = 0xff;

        let value = 0x42;
        cpu.stack_push(value);
        assert_eq!(cpu.sp, 0xfe);
        assert_eq!(cpu.stack_pop(), value);
        assert_eq!(cpu.sp, 0xff);

        let addr = 0x1234;
        cpu.stack_push_address(addr);
        assert_eq!(cpu.sp, 0xfd);
        assert_eq!(cpu.stack_pop_address(), addr);
        assert_eq!(cpu.sp, 0xff);
    }

    #[test]
    fn test_read_address() {
        let mut cpu = new_cpu();
        assert_eq!(cpu.read_address(Immediate(42)), 42);

        cpu.accumulator = 0x69;
        assert_eq!(cpu.read_address(Accumulator), 0x69);

        cpu.write_mem(0x0, 0x1);
        cpu.write_mem(0x1, 0x3);
        cpu.write_mem(0x2, 0x0);
        cpu.write_mem(0x3, 0x1);
        cpu.write_mem(0x4, 0x0);
        cpu.write_mem(0x100, 0x42);
        cpu.write_mem(0x777, 0xff);

        assert_eq!(cpu.read_address(ZeroPage(0x2)), 0x0);
        assert_eq!(cpu.read_address(Absolute(0x777)), 0xff);
        assert_eq!(cpu.read_address(Indirect(0x2)), 0x42);

        cpu.index_x = 0x1;

        assert_eq!(cpu.read_address(ZeroPageX(0x2)), 0x1);
        assert_eq!(cpu.read_address(AbsoluteX(0x776)), 0xff);
        assert_eq!(cpu.read_address(IndirectX(0x2)), 0x3);

        cpu.index_y = 0xfd;

        assert_eq!(cpu.read_address(IndirectY(0x1)), 0x42);
        assert_eq!(cpu.read_address(ZeroPageY(0x4)), 0x3);

        cpu.pc = 0x3;

        assert_eq!(cpu.read_address(Relative(-0x2)), 0x3);
    }

    #[test]
    fn test_write_address() {
        let mut cpu = new_cpu();

        cpu.write_address(AddressingMode::Accumulator, 0x42);
        assert_eq!(cpu.accumulator, 0x42);

        cpu.write_address(ZeroPage(0x2), 0x1);
        assert_eq!(cpu.read_mem(0x2), 0x1);

        cpu.write_address(Absolute(0x777), 0xff);
        assert_eq!(cpu.read_mem(0x777), 0xff);

        cpu.write_address(Indirect(0x2), 0x32);
        assert_eq!(cpu.read_mem(0x1), 0x32);
    }

    #[test]
    fn test_load_data() {
        let mut cpu = new_cpu();
        cpu.load_data(0xff, &[0x21, 0x42, 0x69]);
        assert_eq!(cpu.read_mem(0xff), 0x21);
        assert_eq!(cpu.read_mem(0xff + 1), 0x42);
        assert_eq!(cpu.read_mem(0xff + 2), 0x69);
    }

    #[test]
    fn instruction_adc() {
        let mut cpu = new_cpu();

        cpu.accumulator = 42;
        cpu.run_instruction(Instruction::new(ADC, Immediate(5), 2.into()));

        assert_eq!(cpu.accumulator, 47);
        assert_eq!(cpu.get_status_byte(), 0x34); // 0011 0100

        // TODO
    }

    #[test]
    fn instruction_and() {
        let mut cpu = new_cpu();

        cpu.accumulator = 0b10101010;
        cpu.write_mem(0x100, 0b11010111);
        cpu.write_mem(0x3, 0x00);
        cpu.write_mem(0x4, 0x01);
        cpu.run_instruction(Instruction::new(AND, IndirectY(0x3), 6.into()));

        assert_eq!(cpu.accumulator, 0b10000010);
        assert_eq!(cpu.get_status_byte(), 0b10110100);
    }

    #[test]
    fn instruction_lda() {
        let mut cpu = new_cpu();

        cpu.write_mem(0xfd, 69);
        cpu.run_instruction(Instruction::new(LDA, ZeroPage(0xfd), 3.into()));

        assert_eq!(cpu.accumulator, 69);
        assert_eq!(cpu.get_status_byte(), 0b00110100);
    }

    #[test]
    fn instruction_asl() {
        let mut cpu = new_cpu();

        cpu.accumulator = 0b00000101;

        cpu.run_instruction(Instruction::new(ASL, Accumulator, 2.into()));

        assert_eq!(cpu.accumulator, 0b00001010);
        assert_eq!(cpu.get_status(Status::Carry), false);

        cpu.write_mem(0x42, 0b11000000);

        cpu.run_instruction(Instruction::new(ASL, ZeroPage(0x42), 5.into()));

        assert_eq!(cpu.read_mem(0x42), 0b10000000);
        assert_eq!(cpu.get_status(Status::Carry), true);
    }

    #[test]
    fn instruction_clc() {
        let mut cpu = new_cpu();

        cpu.set_status(Status::Carry, true);
        cpu.run_instruction(Instruction::new(CLC, Implicit, 2.into()));

        assert_eq!(cpu.get_status(Status::Carry), false);
    }

    #[test]
    fn instruction_cpx_cpy() {
        let mut cpu = new_cpu();

        cpu.index_x = 42;
        cpu.write_mem(0x10, 69);

        cpu.run_instruction(Instruction::new(CPX, ZeroPage(0x10), 3.into()));

        assert_eq!(cpu.get_status(Status::Carry), false);

        cpu.index_y = 70;

        cpu.run_instruction(Instruction::new(CPY, ZeroPage(0x10), 3.into()));

        assert_eq!(cpu.get_status(Status::Carry), true);
    }

    #[test]
    fn instruction_lsr() {
        let mut cpu = new_cpu();

        cpu.accumulator = 0b00000101;

        cpu.run_instruction(Instruction::new(LSR, Accumulator, 2.into()));

        assert_eq!(cpu.accumulator, 0b00000010);
        assert_eq!(cpu.get_status(Status::Carry), true);

        cpu.write_mem(0x42, 0b11000000);

        cpu.run_instruction(Instruction::new(LSR, ZeroPage(0x42), 5.into()));

        assert_eq!(cpu.read_mem(0x42), 0b01100000);
        assert_eq!(cpu.get_status(Status::Carry), false);
    }

    #[test]
    fn instruction_pha_plp() {
        let mut cpu = new_cpu();

        cpu.accumulator = 0x42;

        cpu.run_instruction(Instruction::new(PHA, Implicit, 3.into()));
        cpu.run_instruction(Instruction::new(PLP, Implicit, 4.into()));

        assert_eq!(cpu.get_status_byte(), 0x42);
    }

    #[test]
    fn instruction_php_pla() {
        let mut cpu = new_cpu();

        cpu.run_instruction(Instruction::new(PHP, Implicit, 3.into()));
        cpu.run_instruction(Instruction::new(PLA, Implicit, 4.into()));

        assert_eq!(cpu.accumulator, 0x34);
    }

    #[test]
    fn instruction_rol_ror() {
        let mut cpu = new_cpu();

        cpu.accumulator = 0b10110101;

        cpu.run_instruction(Instruction::new(ROL, Accumulator, 2.into()));
        assert_eq!(cpu.accumulator, 0b01101010);
        assert_eq!(cpu.get_status(Status::Carry), true);

        cpu.run_instruction(Instruction::new(ROR, Accumulator, 2.into()));
        assert_eq!(cpu.accumulator, 0b10110101);
        assert_eq!(cpu.get_status(Status::Carry), false);
    }

    #[test]
    fn execution_bmi_jmp() {
        let mut cpu = new_cpu();

        cpu.load_data(
            0,
            &[
                0xa9, 0xfe, // goto1: LDA #$fe
                0x30, 0xfc, // BMI goto1
                0x30, 0x02, // BMI goto2
                0xa9, 0x42, // LDX #$42
                0xa9, 0xfe, // goto2: LDA #$fe
                0x4c, 0x08, 0x00, // JMP goto2
            ],
        );

        cpu.run_next();
        cpu.run_next();

        assert_eq!(cpu.pc, 0x0);

        cpu.pc = 0x4;
        cpu.run_next();
        cpu.run_next();

        assert_eq!(cpu.pc, 0xa);
        assert_eq!(cpu.accumulator, 0xfe);
        assert_eq!(cpu.index_x, 0x0);

        cpu.run_next();

        assert_eq!(cpu.pc, 0x08);
    }

    #[test]
    fn execution_adc_and() {
        let mut cpu = new_cpu();

        cpu.write_mem(0xF0, 0b10010001);

        cpu.load_data(
            0,
            &[
                0x69, 0x2a, // ADC #42
                0x69, 0x09, // ADC #9
                0x35, 0xf0, // AND $f0,X
            ],
        );

        cpu.run_next();

        assert_eq!(cpu.accumulator, 42);
        assert_eq!(cpu.pc, 2);

        cpu.run_next();

        assert_eq!(cpu.accumulator, 51); // 51 == 0b00110011
        assert_eq!(cpu.pc, 4);

        cpu.index_x = 0;
        cpu.run_next();

        assert_eq!(cpu.accumulator, 0b00010001);
        assert_eq!(cpu.pc, 6);
    }
}
